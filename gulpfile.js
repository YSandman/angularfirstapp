var gulp = require('gulp'),
    wiredep = require('wiredep').stream,
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    clean = require('gulp-clean'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    concatCss = require('gulp-concat-css'),
    autoprefixer = require('gulp-autoprefixer'),
    runSequence = require('run-sequence'),
    del = require('del'),
    connect = require('gulp-connect'),
    browserSync = require('browser-sync').create();

//переменная для обновления браузера
var reload = browserSync.reload;

// Connect
gulp.task('connect', ['watch'], function () {
    connect.server({
        port: 9001,
        base: 'app',
        open: false
    });

    browserSync.init({
        notify: false,
        port: 8080,
        server: {
            baseDir: [
                'app'
            ]
        }
    });
});

// Clean
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

// Build
gulp.task('build', ['clean'], function () {
    var assets = useref.assets();

    gulp.src('app/build/**/*')
        .pipe(gulp.dest('dist/build'));

    gulp.src('app/img/**/*')
        .pipe(gulp.dest('dist/img'));

    return gulp.src('app/*.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});

// Bower
gulp.task('bower', function () {
    gulp.src('./app/index.html')
        .pipe(wiredep({
            directory: "app/bower_components"
        }))
        .pipe(gulp.dest('./app'));
});

// Css
gulp.task('css', function () {
    return gulp.src([
        'app/core/**/*.css',
        'app/index.scss',
        'app/templates/**/*.scss'
    ])
        .pipe(plumber({
            errorHandler: function (err) {  //errorHandler  handleError
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(concatCss('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
        .pipe(reload({stream: true}));
});

// clean-app-build
gulp.task('clean-app-build', function () {
    return gulp.src('app/build', {read: false})
        .pipe(clean());
});

// copy-build
gulp.task('copy-build', ['clean-app-build'], function () {
    return gulp.src('build/**/*')
        .pipe(gulp.dest('app/build'));

});

// clean-build
gulp.task('clean-build', function () {
    return gulp.src('build', {read: false})
        .pipe(clean());
});

// component
gulp.task('component', function (callback) {
    runSequence(
        'copy-build',
        'clean-build',
        callback
    );
});

// Watch
gulp.task('watch', function () {
    gulp.watch([
        'app/index.html',
        'app/core/directives/**/*.html',
        'app/templates/**/*.html',

        'app/*.js',
        'app/core/*.js',
        'app/core/constants/constants.js',
        'app/core/directives/**/*.js',
        'app/core/factories/*.js',
        'app/core/filters/*.js',
        'app/core/services/*.js',
        'app/templates/*.js',
        'app/templates/**/*.js'
    ]).on('change', reload);
    gulp.watch([
        'app/index.scss',
        'app/templates/**/*.scss',
        'app/core/**/*.css',
        'app/css/*.css'
    ], ['css']);
    gulp.watch('bower.json', ['bower']);
});

//default
gulp.task('default', ['css', 'bower', 'connect']);