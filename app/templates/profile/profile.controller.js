varOfProject
    .controller('Profile', [
        '$state',
        '$stateParams',
        'user',
        function ($state, $stateParams, user) {
            var vm = this;

            vm.userData = user.getInfo();
        }]
    )
;