varOfProject
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        '$mdThemingProvider',
        function ($stateProvider, $urlRouterProvider, $mdThemingProvider) {
            $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
            $urlRouterProvider.otherwise('/home');

            $stateProvider
                .state('home', {
                    url: '/home',
                    templateUrl: 'templates/home/home.html',
                    controller: 'Home',
                    controllerAs: 'vm',
                    resolve: {
                        desktopInfo: function () {
                            console.log('at first - resolve');
                            return [
                                {
                                    'name': 'Broccoli',
                                    'type': 'Brassica',
                                    'id': 0
                                },
                                {
                                    'name': 'Cabbage',
                                    'type': 'Brassica',
                                    'id': 2
                                },
                                {
                                    'name': 'Carrot',
                                    'type': 'Umbellif',
                                    'id': 10
                                },
                                {
                                    'name': 'Lettuce',
                                    'type': 'Composite',
                                    'id': 454
                                },
                                {
                                    'name': 'Spinach',
                                    'type': 'Goosefoot',
                                    'id': 233445
                                }
                            ];
                        }
                    }
                })
                .state('profile', {
                    url: '/profile',
                    templateUrl: 'templates/profile/profile.html',
                    controller: 'Profile',
                    controllerAs: 'vm'
                })
                .state('vegetable', {
                    url: '/vegetable/:id',
                    templateUrl: 'templates/vegetable/vegetable.html',
                    controller: 'Vegetable',
                    controllerAs: 'vm'
                })
                .state('new-page', {
                    url: '/new-page',
                    templateUrl: 'templates/new_page/new_page.html',
                    controller: 'NewPage',
                    controllerAs: 'vm'
                });
        }]
    );
