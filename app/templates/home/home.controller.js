varOfProject
    .controller('Home', [
        '$scope',
        '$stateParams',
        'user',
        '$mdSidenav',
        'desktopInfo',
        '$state',
        function ($scope, $stateParams, user, $mdSidenav, desktopInfo, $state) {
            var vm = this;
            console.log('then control home page');

            vm.vegetables = desktopInfo;

            vm.getMoreInfo = function (item) {
                $state.go('vegetable', {id: item});
            }
        }]
    );