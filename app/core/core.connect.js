var directives = angular.module('directives.module', []);
var factories = angular.module('factories.module', []);
var services = angular.module('services.module', []);
var filters = angular.module('filters.module', []);

angular.module('core', [
    'ngMaterial',
    'ngStorage',
    'ui.router',
    'toastr',

    'url.module',
    'http.module',
    'constants.module',
    'factories.module',
    'services.module',
    'directives.module'
]);