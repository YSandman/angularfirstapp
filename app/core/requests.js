angular
    .module('http.module', [])
    .factory('http', [
        '$http', '$sessionStorage', 'toast', '$q', '$rootScope',
        function ($http, $sessionStorage, toast, $q, $rootScope) {
            // http://stackoverflow.com/questions/16483873/angularjs-http-post-file-and-form-data
            return {
                get: function (url, data) {
                    return request('GET', url, data);
                },
                post: function (url, data) {
                    return request('POST', url, data);
                },
                delete: function (url, data) {
                    return request('DELETE', url, data);
                },
                put: function (url, data) {
                    return request('PUT', url, data);
                },
                file: function (url, data) {
                    return requestFile(url, data);
                },
                fileTransfer: function (status, url, fileName, path, data) {
                    return requestFileTransfer(status, url, fileName, path, data);
                }
            };

            function requestFileTransfer(status, url, fileName, path, data) {
                $rootScope.loading = true;
                var ft = new FileTransfer(),
                    deferred = $q.defer(),
                    downloadPath,
                    rootFolder,
                    requestUrl;

                if ($sessionStorage.auth_key) {
                    requestUrl = url + '?auth_key=' + $sessionStorage.auth_key;
                }
                else {
                    requestUrl = url;
                }


                if (status === 'upload') {
                    console.log(data);
                    ft.upload(path, encodeURI(requestUrl), fileSuccess(deferred), fileError(deferred),
                        {
                            fileName: fileName,
                            fileKey: 'file',
                            mimeType: 'audio/x-m4a',
                            httpMethod: 'POST',
                            chunkedMode: false,
                            params: data
                        }
                    );
                }

                if (status === 'download') {
                    rootFolder = cordova.file.externalRootDirectory;
                    downloadPath = rootFolder + 'Download/' + fileName;

                    ft.download(encodeURI(requestUrl), downloadPath, fileSuccess(deferred), fileError(deferred), false,
                        {params: data}
                    );
                }

                $rootScope.loading = false;

                return deferred.promise;
            }

            function fileSuccess(def) {
                return function (result) {
                    console.log(result);
                    $rootScope.loading = false;
                    def.resolve(result);
                };
            }

            function fileError(def) {
                return function (error) {
                    console.log(error);
                    $rootScope.loading = false;
                    def.reject(error);
                };
            }

            function request(method, url, data) {
                $rootScope.loading = true;

                var config = {
                    dataType: 'json',
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                };

                if (method === 'GET') {
                    config.params = data;
                }
                else {
                    config.data = data;
                }

                if ($sessionStorage.auth_key) {
                    config.url = url + '?auth_key=' + $sessionStorage.auth_key;
                }
                else {
                    config.url = url;
                }

                return $http(config)
                    .then(requestComplete)
                    .catch(requestFailed);
            }

            function requestFile(url, data) {
                $rootScope.loading = true;

                var config = {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                if ($sessionStorage.auth_key) {
                    url = url + '?auth_key=' + $sessionStorage.auth_key;
                }

                return $http.post(url, data, config)
                    .then(requestComplete)
                    .catch(requestFailed);
            }

            function requestFailed(err) {
                console.info('error', err.config.url, err);

                if (err.data === null || !err.data.error) {
                    if (err.status === 200) {
                        toast.def('Server Error: ' + err.data);
                        // toast.def('Ошибка сервера: ' + err.data);
                    }
                    else if (err.status === 0) {
                        toast.def('Server unavailable');
                        // toast.def('Сервер недоступен');
                    }
                    else if (err.status === -1) {
                        toast.def('No internet connection');
                        // toast.def('Отсутстувет подключение к интернету');
                    }
                    else if (err.status === 500) {
                        toast.def('Server Error: ' + err.status + ' ' + err.data.message);
                    }
                    else {
                        toast.def('Server Error: ' + err.status + ' ' + err.statusText);
                    }
                }
                else {
                    toast.def('Error: ' + err.data.error);
                }

                $rootScope.loading = false;

                return $q.reject(err.data.error);
            }

            function requestComplete(response) {
                var promise = $q.defer();

                console.info('response complete', response.config.url + '\n', response.data);

                if (!response.data.error) {
                    promise.resolve(response.data);
                }
                else {
                    promise.reject(response);
                }

                $rootScope.loading = false;
                // set timeout on requests

                return promise.promise;
            }
        }]
    );