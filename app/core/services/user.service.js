services
    .service('user', user);

user.$inject = ['http', 'url', '$sessionStorage'];

function user(http, url, $sessionStorage) {

    return {
        login: login,
        getInfo: getUserInfo,
        logout: logout
    };

    function login(data, callback) {
        // test variation
        $sessionStorage.user = data;
        //
        return http.post(url.user.login, data)
            .then( function (res) {
                callback(res);
            });
    }

    function getUserInfo() {
        // test variation
        return {
            name: 'User Service',
            pass: '********',
            height: '170',
            weight: '60'
        };
        //
    }

    function logout() {
        // test variation
        delete $sessionStorage.user;
        console.log($sessionStorage.user);
        //
        delete $sessionStorage.auth_key;
    }
}