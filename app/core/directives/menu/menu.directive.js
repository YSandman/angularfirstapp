directives
    .directive('appMenu', function () {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/menu/menu.directive.html",
            transclude: true,
            scope: {},
            controller: function ($scope, user, $mdSidenav) {
                var vm = this;
                $scope.toggleLeft = buildToggler('left');
                $scope.toggleRight = buildToggler('right');

                function buildToggler(componentId) {
                    return function() {
                        $mdSidenav(componentId).toggle();
                    };
                }
            }
        }
    });
