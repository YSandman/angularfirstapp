directives
    .directive('appHeader', function ($mdDialog) {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/header/header.directive.html",
            transclude: true,
            scope: {},
            controller: function ($scope, user, $mdSidenav) {
                var vm = this;

                $scope.toggleLeft = buildToggler('left');
                $scope.toggleRight = buildToggler('right');

                function buildToggler(componentId) {
                    return function () {
                        $scope.$root.isMenuHidden = !$scope.$root.isMenuHidden;
                        $mdSidenav(componentId).toggle();
                    };
                }

                $scope.$root.showLoginForm = function (ev) {
                    $mdDialog.show({
                        controller: LoginDialogController,
                        templateUrl: 'core/directives/header/login.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: false
                    })
                        .then(function (answer) {
                            $scope.$root.user = answer;
                            user.login(answer, function (data) {
                                console.log("Response on login");
                            });
                            console.log(answer);
                        }, function () {
                            console.log('Canceled');
                        });
                };

                $scope.$root.logOut = function () {
                    console.log("log out");
                    user.logout();
                    delete $scope.$root.user;
                };

                function LoginDialogController($scope, $mdDialog) {
                    $scope.user = {};

                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };

                    $scope.answer = function(answer) {
                        $mdDialog.hide(answer);
                    };
                }
            }
        }
    });