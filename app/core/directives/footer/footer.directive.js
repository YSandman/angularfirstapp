directives
    .directive('appFooter', function () {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/footer/footer.directive.html",
            transclude: true,
            scope: {

            },
            controller: function ($scope) {

            }
        }
    });