angular
    .module('url.module', [])
    .factory('url', function () {
        var baseUrl = 'http://some.server.name/api/web/v1/';
        // var baseUrl = 'http://10.95.106.53/api/web/v1/';

        return {
            user: {
                login:          baseUrl + 'default/login',
                params:         baseUrl + 'user/user-info'
            }
        };
    });