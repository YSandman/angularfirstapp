factories
    .factory('toast', function ($mdToast) {
        return {
            def: def
        };

        function def(text) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(text)
                    .position('top right')
                    .hideDelay(3000)
            );
        }
    });