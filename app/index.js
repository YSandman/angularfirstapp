var varOfProject = angular.module('nameOfProject', [
    'ngMaterial',
    'ngAnimate',
    'ui.router',
    'ngStorage',

    'core'
])
    .run([
        '$sessionStorage',
        '$rootScope',
        function ($sessionStorage, $rootScope) {
            $rootScope.isMenuHidden = false;
            $rootScope.user = false;

            if ($sessionStorage.user) {
                $rootScope.user = $sessionStorage.user;
                console.log('Hello, ' + $rootScope.user.name + '!');
            }
        }
    ]);